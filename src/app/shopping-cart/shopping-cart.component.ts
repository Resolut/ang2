import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../product.model';
import { CartProduct } from '../cartproduct.model';
import { ShoppingCartService } from '../shoppingcart.service';
import { PopupComponent } from '../popup/popup.component';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  @ViewChild(PopupComponent) popupComponent: PopupComponent;
  private cartProducts: CartProduct[] = [];
  private totalPrice = 0;
  constructor(private _cartService: ShoppingCartService) {}

  ngOnInit() {
    this.cartProducts = this._cartService.getProducts();
    this.totalPrice = this._cartService.getTotalPrice();

  }

  checkoutCart() {
    this.popupComponent.doShow({type: 'warning', text: 'Not implement yet!'});
  }

}
