import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { PopupComponent } from './popup/popup.component';
import { Popup } from './popup.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/styles/common.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(PopupComponent) popupComponent: PopupComponent;

  ngOnInit() {
    this.popupComponent.doShow({text: 'App great works!'});
  }

}
