import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { Product } from '../product.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductsService]
})
export class ProductsComponent implements OnInit {
  title = 'List of Products';
  private pending: boolean = false;
  private products: Product[];
  constructor(private _productService: ProductsService) {}

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.pending = true;
    this._productService.getProducts()
      .subscribe(
        products => {
          this.products = products;
          this.pending = false;
        },
        () => this.pending = false);
  }
}
