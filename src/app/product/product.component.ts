import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../product.model';
import { ShoppingCartService } from '../shoppingcart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['../../assets/styles/products.scss']
})
export class ProductComponent implements OnInit {
  @Input() product: Product;

  constructor(private _cartService: ShoppingCartService) {}

  ngOnInit() {
  }

  public addToCart() {
    this._cartService.addProduct(this.product);
  }
}
