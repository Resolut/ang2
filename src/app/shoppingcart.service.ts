import { Injectable, Input } from '@angular/core';
import { Product } from './product.model';
import { CartProduct } from './cartproduct.model';

@Injectable()
export class ShoppingCartService {
  private totalPrice = 0;
  cartProducts: CartProduct[] = [];

  private setTotalPrice() {
    this.totalPrice = this.cartProducts.reduce((sum, currentProduct) =>
      sum + currentProduct.product.price * currentProduct.quantity,
      0);
  }

  public getTotalPrice() {
    return this.totalPrice;
  }

  public addProduct(product: Product) {
    const foundProduct = this.cartProducts.find(currentProduct =>
      currentProduct.product.id === product.id);

    if (foundProduct) {
      foundProduct.quantity++;
    } else {
      const cartProduct = new CartProduct(product, 1);
      this.cartProducts.push(cartProduct);
    }
    this.setTotalPrice();
  }

  public getProducts() {
    return this.cartProducts;
  }
}
