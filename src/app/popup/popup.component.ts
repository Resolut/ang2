import { Component, Input, OnInit } from '@angular/core';
import { Popup } from '../popup.model';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  private popupList: Popup[] = [];

  ngOnInit() {
  }

  public doShow(popupParam: Popup) {
    this.popupList.push(new Popup(popupParam));
  }

}
