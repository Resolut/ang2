var mock = require('./mock.products.json');
var express = require('express');
var app = express();

app.get('/api/test', function (req, res) {
  res.send('API server works!');
});
app.get('/api/products', function (req, res) {
  setTimeout(() => {  res.send(mock); }, 1500);
});

app.listen(3000, function () {
  console.log('API server listening on port 3000!');
});